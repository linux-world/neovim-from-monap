require '2023/options'
require '2023/keymap'
require '2023/plugin'
require '2023/colorscheme'
require '2023/cmp'
require '2023/lsp'
require '2023/telescope'
require '2023/treesister'
require '2023/autopairs'
require '2023/comment'
require '2023/gitsigns'
require '2023/nvim-tree'
require '2023/bufferline'
require '2023/lualine'
require '2023/toggleterm'
