local status_ok, configs = pcall(require, "nvim-treesitter.configs")
if not status_ok then
  return
end

configs.setup {
  ensure_installed = "all", -- 安装所有解析器
 -- ensure_installed = {
 --   "html","css","vim","lua","javascript","typescript",
 --   "bash","c","cmake","cpp","go","java","json","markdown",
 --   "php","python","scss","sql","tsx","vue","yaml"
 -- },
  sync_install = false, -- 异步安装
  ignore_install = { "phpdoc","jsonc","fusion"}, -- 不需要安装的解析器
  
  autopairs = {
    enable = true,
  },
  
  -- 启用代码高亮
  highlight = {
    enable = true, -- false 将禁用整个扩展
    disable = { "" }, -- 语言解析器禁用列表
    additional_vim_regex_highlighting = true, -- 给 vim 添加额外的语法高亮支持
  },
  
  -- 启用基于Treesitter的代码格式化(=), 智能缩进！
  indent = { enable = true, disable = { "yaml" } },
  
 context_commentstring = {
    enable = true,
    enable_autocmd = false,
  },
}
