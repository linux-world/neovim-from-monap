-- aurora codemonkey darkplus ferrum lunar onedark onedarker onedarkest onenord spacedark system76 tomorrow
local colorscheme = "system76"

vim.g.transparent_background = false

local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
  vim.notify("colorscheme " .. colorscheme .. " not found !")
  return
end
