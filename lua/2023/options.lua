-- :help options 查看所有参数
local options = {
  -- 文件编码
  fileencoding = "utf-8",
  -- jkhl键移动时光标上下方保留8行
  scrolloff = 8,
  sidescrolloff = 8,
  -- 使用行号
  number = true,
  -- 使用相对行号（以当前行为基准）
  relativenumber = true,
  -- 当前行高亮
  cursorline = true,
  -- 显示左侧图标指示列
  signcolumn = "yes",
  -- 右侧参考线，超过表示代码太长考虑换行
  -- colorcolumn = "80",
  -- 缩进2个空格等于一个Tab
  tabstop = 2,
  softtabstop = 2,
  shiftround = true,
  -- 为每个缩进插入的空格数
  shiftwidth = 2,
  -- 空格代替tab
  expandtab = true,
  -- 新行对齐当前行
  autoindent = true,
  smartindent = true,
  -- 搜索时忽略大小写,除非包含大写
  ignorecase = true,
  smartcase = true,
  -- 搜索匹配项高亮
  hlsearch = true,
  -- 边输入边搜索
  incsearch = true,
  -- 使用增强状态栏后不再需要 vim 的模式提示 
  --  showmode = false,
  -- 命令行高为2,提供足够的显示空间
  cmdheight = 2,
  -- 当文件被外部程序修改时，自动加载
  autoread = true,
  -- 禁止折行
  wrap = false,
  -- 允许隐藏被修改过的buffer
  hidden = true,
  -- 允许在 neovim 中使用鼠标
  mouse = "a",
  -- 禁止创建备文件
  backup = false,
  -- 如果文件正在由其他程序编辑（或在使用其他程序编辑时写入文件），则不允许对其进行编辑
  writebackup = false,
  -- 禁止创建交换文件
  swapfile = false,
  -- 更快完成（默认为 4000 ms）
  updatetime = 300,
  -- 等待映射序列完成的时间（毫秒）
  timeoutlen = 100,
  -- 强制所有水平拆分位于当前窗口下方
  splitbelow = true,
  -- 强制所有垂直拆分位于当前窗口右侧
  splitright = true,
  -- 自动补全不自动选中
  completeopt = { "menuone", "noselect" },
  -- 样式
  termguicolors = true,
  -- 不可见字符的显示，这里只把空格显示为一个点
  list = true,
  listchars = "space:·",
  -- 补全增强
  wildmenu = true,
  -- 弹出菜单的高度
  pumheight = 10,
  -- 永远显示 tabline
  showtabline = 2,
  -- 允许 enovim 接受外部系统粘贴
  clipboard = "unnamedplus",
  -- markdown 文件中可以显示 `` 符号
  conceallevel = 0,
  -- 设置 term gui 颜色 (大多数终端支持此功能)
  termguicolors = true,
  -- 启用永久撤销
  undofile = true,
  -- 将数字列宽设置为2（默认为4）
  numberwidth = 4,
  -- 图形 neovim 应用中使用的字体
  guifont = "monospace:h17",
}

for k, v in pairs(options) do
  vim.opt[k] = v
end

vim.opt.shortmess:append "c"               -- 禁止在下方显示一些啰嗦的提示

-- vim.cmd "xx" 字符串将被当作vim命令执行
vim.cmd "set whichwrap+=<,>,[,],h,l"       -- 光标在行结尾左右键可以可以跳到下一行
vim.cmd [[set iskeyword+=-]]
vim.cmd [[set formatoptions-=cro]]         -- TODO: 似乎没起作用
