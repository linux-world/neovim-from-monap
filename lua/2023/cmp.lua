-- 引入cmp
local cmp_status_ok, cmp = pcall(require, "cmp")
if not cmp_status_ok then
  return
end

-- 引入snip
local snip_status_ok, luasnip = pcall(require, "luasnip")
if not snip_status_ok then
  return
end

-- 延迟加载，这样代码段就可以按需加载而不是一次全部加载（可能会干扰延迟加载luasnip本身)
require("luasnip/loaders/from_vscode").lazy_load()


local check_backspace = function()
  local col = vim.fn.col "." - 1
  return col == 0 or vim.fn.getline("."):sub(col, col):match "%s"
end


-- 图标对应列表
local kind_icons = {
  Text = "",
  Method = "m",
  Function = "",
  Constructor = "",
  Field = "",
  Variable = "",
  Class = "",
  Interface = "",
  Module = "",
  Property = "",
  Unit = "",
  Value = "",
  Enum = "",
  Keyword = "",
  Snippet = "",
  Color = "",
  File = "",
  Reference = "",
  Folder = "",
  EnumMember = "",
  Constant = "",
  Struct = "",
  Event = "",
  Operator = "",
  TypeParameter = "",
}
-- 寻找更多字体图标: https://www.nerdfonts.com/cheat-sheet



-- cmp 具体设置
cmp.setup {
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body) -- 选择 `luasnip` 作为代码段引擎 
    end,
  },

  -- 快捷键映射
  mapping = {
    ["<C-k>"] = cmp.mapping.select_prev_item(),    -- 提示列表上一条
    ["<C-j>"] = cmp.mapping.select_next_item(),    -- 提示列表下一条
    ["<C-b>"] = cmp.mapping(cmp.mapping.scroll_docs(-1), { "i", "c" }), -- 提示文档滚动上一页
    ["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(1), { "i", "c" }),  -- 提示文档滚动下一页
    ["<C-p>"] = cmp.mapping(cmp.mapping.complete(), { "i", "c" }),      -- 未输入任何字符时显示全部提示
    ["<C-y>"] = cmp.config.disable, -- 禁用掉默认的 `<C-y>` 映射
    ["<C-e>"] = cmp.mapping {       -- 退出提示
      i = cmp.mapping.abort(),
      c = cmp.mapping.close(),
    }, 
    ["<CR>"] = cmp.mapping.confirm { select = true }, -- 回车键确认选择,未选择默认第一项
    ["<Tab>"] = cmp.mapping(function(fallback)        -- tab 键的条件映射
      if cmp.visible() then
        cmp.select_next_item()                        -- 选择下一项
      elseif luasnip.expandable() then
        luasnip.expand()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      elseif check_backspace() then
        fallback()  -- 否则为 tab 键的常规操作
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
    ["<S-Tab>"] = cmp.mapping(function(fallback)      -- shfit tab 的条件映射
      if cmp.visible() then
        cmp.select_prev_item()                        -- 选择上一项
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, {
      "i",
      "s",
    }),
  },

  -- 格式化
  formatting = {
    fields = { "kind", "abbr", "menu" },
    format = function(entry, vim_item)
      -- Kind icons 
      vim_item.kind = string.format("%s", kind_icons[vim_item.kind])
      -- vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind) -- 将图标与项目类型的名称联系在一起
      vim_item.menu = ({
        nvim_lsp = "[LSP]",
        luasnip = "[Snippet]",
        buffer = "[Buffer]",
        path = "[Path]",
      })[entry.source.name]
      return vim_item
    end,
  },

  -- 提示的类型先后顺序
  sources = {
    { name = "nvim_lsp" },
    { name = "luasnip" },
    { name = "buffer" },
    { name = "path" },
  },
  confirm_opts = {
    behavior = cmp.ConfirmBehavior.Replace,
    select = false,
  },
  window = {
    documentation = {
      border = { "╭", "─", "╮", "│", "╯", "─", "╰", "│" },
    },
  },
  experimental = {
    ghost_text = true,    -- 虚拟提示
    native_menu = false,
  },
}
