-- 检查 LSP installer（开始安装语言服务器）
local status_ok, lsp_installer = pcall(require, "nvim-lsp-installer")
if not status_ok then
  return
end

local lspconfig = require("lspconfig")
-- 各类语言支持
local servers = {
  "jsonls",                -- json
  "sumneko_lua",           -- lua
  "pyright",               -- python
}

lsp_installer.setup {
  ensure_installed = servers
}

for _, server in pairs(servers) do
  -- 检查附加功能和处理程序
  local opts = {
    on_attach = require("2023.lsp.handlers").on_attach,
    capabilities = require("2023.lsp.handlers").capabilities,
  }
  -- 各类语言配置
  local has_custom_opts, server_custom_opts = pcall(require, "2023.lsp.settings." .. server)
  if has_custom_opts then
    opts = vim.tbl_deep_extend("force", server_custom_opts, opts)
  end
  lspconfig[server].setup(opts)
end
