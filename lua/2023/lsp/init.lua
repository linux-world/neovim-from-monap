-- 检查 lspconfig
local status_ok, _ = pcall(require, "lspconfig")
if not status_ok then
  return
end

-- 引用
require "2023.lsp.configs"
require("2023.lsp.handlers").setup() -- 调用setup函数
require "2023.lsp.null-ls"
