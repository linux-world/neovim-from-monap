local null_ls_status_ok, null_ls = pcall(require, "null-ls")
if not null_ls_status_ok then
 return
end

-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/formatting
-- 内置大多数语言的格式化配置
local formatting = null_ls.builtins.formatting
-- https://github.com/jose-elias-alvarez/null-ls.nvim/tree/main/lua/null-ls/builtins/diagnostics
local diagnostics = null_ls.builtins.diagnostics

null_ls.setup({
 debug = false,
 sources = {
  -- js prettier
  formatting.prettier.with({ extra_args = { "--no-semi", "--single-quote", "--jsx-single-quote" } }),
  -- py black
  formatting.black.with({ extra_args = { "--fast" } }),
  -- java google_java_format
  formatting.google_java_format,
  formatting.stylua,
    -- diagnostics.flake8
 },
})
