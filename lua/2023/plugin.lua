local fn = vim.fn

-- 自动安装 packer （neovim 插件包管理器）
-- neovim data目录默认为 ~/.local/share/nvim
-- 即插件全路径为：~/.local/share/nvim/site/pack/packer/start/packer.nvim
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
  PACKER_BOOTSTRAP = fn.system {
    "git",
    "clone",
    "--depth",
    "1",
    "https://github.com/wbthomason/packer.nvim",
    install_path,
  }
  print "Installing packer close and reopen Neovim..."
  vim.cmd [[packadd packer.nvim]]
end

-- 自动命令，在保存 plugins.lua 文件时重新加载neovim（按照 plugins.lua 更新插件）
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- 使用受保护的调用，以便我们在首次使用时不会出错
local status_ok, packer = pcall(require, "packer")
if not status_ok then
  vim.notify("please check the working status of packer!")
  return
end

-- 让 packer 使用弹出窗口（默认拆分窗口显示）
packer.init {
  display = {
    open_fn = function()
      return require("packer.util").float { border = "rounded" }
    end,
  },
}

-- 声明你需要的所有插件
return packer.startup(function(use)
  -- My plugins here
  use "wbthomason/packer.nvim"               -- 使 packer 管理它自己(保持packer版本更新)
  use "nvim-lua/popup.nvim"                  -- Neovim中 vim Popup API 的实现
  use "nvim-lua/plenary.nvim"                -- 提供了大量有用的 lua 函数，你可以在其他的插件或自定义插件中引用它们
  use "windwp/nvim-autopairs"                -- 自动不全，与 cmp 和 treesitter 配合
  use "numToStr/Comment.nvim"                -- 轻松注释内容
  use "kyazdani42/nvim-web-devicons"
  use "kyazdani42/nvim-tree.lua"
  use "akinsho/bufferline.nvim"
  use "moll/vim-bbye"
  use "nvim-lualine/lualine.nvim"
  use "akinsho/toggleterm.nvim"
--  use "ahmedkhalf/project.nvim"
--  use "lewis6991/impatient.nvim"
-- use "lukas-reineke/indent-blankline.nvim"
--  use "goolord/alpha-nvim"
--  use "antoinemadec/FixCursorHold.nvim" -- This is needed to fix lsp doc highlight
--  use "folke/which-key.nvim"

  -- Colorschemes
  use "lunarvim/colorschemes"                 --  你可以尝试的一系列配色方案

  -- cmp plugins
  use "hrsh7th/nvim-cmp" -- The completion plugin
  use "hrsh7th/cmp-buffer" -- buffer completions
  use "hrsh7th/cmp-path" -- path completions
  use "hrsh7th/cmp-cmdline" -- cmdline completions
  use "saadparwaiz1/cmp_luasnip" -- snippet completions
  use "hrsh7th/cmp-nvim-lsp"

  -- snippets
  use "L3MON4D3/LuaSnip" --snippet engine
  use "rafamadriz/friendly-snippets" -- a bunch of snippets to use

  -- LSP
  use "neovim/nvim-lspconfig" -- enable LSP
  use "williamboman/nvim-lsp-installer" -- simple to use language server installer
  use "tamago324/nlsp-settings.nvim" -- language server settings defined in json for
  use "jose-elias-alvarez/null-ls.nvim" -- for formatters and linters

  -- Telescope
  use "nvim-telescope/telescope.nvim"

  -- Treesitter
  use {
   "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
  }
--  use "JoosepAlviste/nvim-ts-context-commentstring"

  -- Git
  use "lewis6991/gitsigns.nvim"

  -- Automatically set up your configuration after cloning packer.nvim
  -- Put this at the end after all plugins
  if PACKER_BOOTSTRAP then
    require("packer").sync()
  end
end)
