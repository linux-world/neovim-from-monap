local opts = { noremap = true, silent = true }

local term_opts = { silent = true }

-- 缩短函数名
local keymap = vim.api.nvim_set_keymap

-- leader可以看做是vim快捷键的触发，通过leader+其他按键达到快捷键的作用
-- 将 空格 映射为 leader 键（默认leader键是 \）
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "


-- 模式大纲
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",


-------------------- 1.Normal模式 --------------------
-- 快速调出资源管理器
keymap("n","<leader>e",":Lex 30<cr>",opts)

-- 拆分窗口导航
-- sv 水平分屏
-- sh 垂直分屏
-- sc 关闭当前分屏 (s = close)
-- so 关闭其他分屏 (o = other)
-- s> s< s= sj sk 分屏比例控制
keymap("n", "sv", ":vsp<CR>", opts)
keymap("n", "sh", ":sp<CR>", opts)
keymap("n", "sc", "<C-w>c", opts)
keymap("n", "so", "<C-w>o", opts)

-- 各窗口间跳转
keymap("n", "<C-Left>", "<C-w>h", opts)
keymap("n", "<C-Down>", "<C-w>j", opts)
keymap("n", "<C-Up>", "<C-w>k", opts)
keymap("n", "<C-Right>", "<C-w>l", opts)

-- 设置窗口比例
keymap("n", "<C-s>", ":resize -2<CR>", opts)
keymap("n", "<C-w>", ":resize +2<CR>", opts)
keymap("n", "<C-a>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-d>", ":vertical resize +2<CR>", opts)

-- 当前窗口 buffer 导航
keymap("n", "<S-l>", ":bnext<CR>", opts)
keymap("n", "<S-h>", ":bprevious<CR>", opts)

-- 上下移动文本
keymap("n", "<A-Down>", "<Esc>:m .+1<CR>==gi", opts)
keymap("n", "<A-Up>", "<Esc>:m .-2<CR>==gi", opts)


-------------------- 2.Insert 模式 --------------------
-- 同时输入jk进入Normal模式
keymap("i", "jk", "<ESC>", opts)


-------------------- 3.Visual 模式 --------------------
-- 进行缩进
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

-- 上下移动文本
keymap("v", "<A-j>", ":m .+1<CR>==", opts)
keymap("v", "<A-k>", ":m .-2<CR>==", opts)

-- 快速粘贴（复制为 `y`）
keymap("v", "p", '"_dP', opts)


----------------- 4.Visual Block 模式 ----------------
-- 上下移动文本
keymap("x", "J", ":move '>+1<CR>gv-gv", opts)
keymap("x", "K", ":move '<-2<CR>gv-gv", opts)
keymap("x", "<A-j>", ":move '>+1<CR>gv-gv", opts)
keymap("x", "<A-k>", ":move '<-2<CR>gv-gv", opts)


------------------- 5.Terminal模式 -------------------
-- 内置 terminal 导航（输入 :terminal 进入）
-- keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
-- keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
-- keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
-- keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)


-------------------6.Other----------------------------
-- 文件搜索（telescope 支持）
keymap("n","<c-f>","<cmd>Telescope find_files<cr>",opts)
keymap("n","<leader>f","<cmd>lua require'telescope.builtin'.find_files(require('telescope.themes').get_dropdown({previewer = false}))<cr>",opts)
-- 全文搜索（telescope 支持）
keymap("n","<c-t>","<cmd>Telescope live_grep<cr>",opts)
